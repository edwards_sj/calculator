<?php

/**
 * Created by PhpStorm.
 * User: edwards_sj
 * Date: 21/02/2015
 * Time: 11:12
 */

require('op.php');

class opTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var op
     */
    protected $op;

    public function testPlusPrecedence()
    {
        $this->getOp()->set('+');
        $this->assertEquals(1, $this->getOp()->precedence());
    }

    public function testMultiplyPrecedence()
    {
        $this->getOp()->set('*');
        $this->assertEquals(2, $this->getOp()->precedence());
    }

    public function testAddCalculation()
    {
        $this->getOp()->set('+');
        $this->assertEquals(1115.4, $this->getOp()->run(1113.4, 2));
    }

    public function testDivideByZero()
    {
        $this->getOp()->set('/');

        try
        {
            $this->getOp()->run(3, 0);
        } catch (Exception $e)
        {
            if ($e->getMessage() == 'Attempt to divide by zero')
            {
                return;
            }
        }

        $this->fail('Exception not raised');
    }

    public function testInvalidOpCode()
    {
        $this->assertSame(FALSE, $this->getOp()->set('XXXXXXX'));
    }

    /**
     * @return op
     */
    public function getOp()
    {
        if (!$this->op)
        {
            $this->op = new op();
        }
        return $this->op;
    }


}
