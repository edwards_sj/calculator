<?php

/**
 * Created by PhpStorm.
 * User: edwards_sj
 * Date: 21/02/2015
 * Time: 12:04
 */

require('calculator.php');

class calculatorTest extends PHPUnit_Framework_TestCase
{
    public function testCalculation1()
    {
        $calculator = new calculator();
        $calculator->push(1);
        $calculator->push('+');
        $calculator->push(1);
        $calculator->push('*');
        $calculator->push(3);
        $calculator->push('+');
        $calculator->push(3);

        $this->assertEquals(7, $calculator->run());

    }

    public function testCalculation2()
    {
        $calculator = new calculator();
        $calculator->push(1);
        $calculator->push('*');
        $calculator->push(4);
        $calculator->push('+');
        $calculator->push(6);
        $calculator->push('*');
        $calculator->push(3);

        $this->assertEquals(22, $calculator->run());

    }

    public function testCalculation3()
    {
        $calculator = new calculator();
        $calculator->push(1);
        $calculator->push('*');
        $calculator->push(4);
        $calculator->push('+');
        $calculator->push(6);
        $calculator->push('*');
        $calculator->push(3);
        $calculator->push('/');
        $calculator->push(4);
        $calculator->push('+');
        $calculator->push(6);
        $calculator->push('*');
        $calculator->push(2);
        $this->assertEquals(20.5, $calculator->run());

    }

    public function testCalculation4()
    {
        $calculator = new calculator();
        $calculator->push(1);
        $calculator->push('*');
        $calculator->push(4);
        $calculator->push('+');
        $calculator->push(6);
        $calculator->push('*');
        $calculator->push(3);
        $calculator->push('/');
        $calculator->push(4);
        $calculator->push('*');
        $calculator->push(6);
        $calculator->push('+');
        $calculator->push(2);
        $this->assertEquals(33, $calculator->run());

    }

    public function testCalculation5()
    {
        $calculator = new calculator();
        $calculator->push(1.11);
        $calculator->push('*');
        $calculator->push(4.11);
        $calculator->push('+');
        $calculator->push(5.11);
        $this->assertEquals(9.6721, $calculator->run());

    }

    public function testCalculation6()
    {
        $calculator = new calculator();
        $calculator->push(7);
        $calculator->push('/');
        $calculator->push(23);
        $calculator->push('*');
        $calculator->push(6);
        $calculator->push('/');
        $calculator->push(5);
        $this->assertEquals(0.365, round($calculator->run(), 3));

    }

    public function testCalculation7()
    {
        $calculator = new calculator();
        $calculator->push(22);
        $calculator->push('*');
        $calculator->push(6);
        $calculator->push('+');
        $calculator->push(7);
        $calculator->push('-');
        $calculator->push(19);
        $calculator->push('/');
        $calculator->push(2);
        $this->assertEquals(129.5, $calculator->run());

    }

    public function testInvalidStack()
    {
        $calculator = new calculator();
        $calculator->push('+');
        $calculator->push('+');
        try
        {
            $calculator->run();
        } catch (Exception $e)
        {
            if ($e->getMessage() == 'Invalid stack')
            {
                return;
            }
        }

        $this->fail('Exception not raised');
    }

    public function testEmptyStack()
    {
        $calculator = new calculator();
        try
        {
            $calculator->run();
        } catch (Exception $e)
        {
            if ($e->getMessage() == 'Invalid stack')
            {
                return;
            }
        }

        $this->fail('Exception not raised');
    }
}
