<?php

/**
 * Created by PhpStorm.
 * User: edwards_sj
 * Date: 21/02/2015
 * Time: 10:56
 */
class op
{
    private static $validOps = ['+' => 'add', '-' => 'subtract', '*' => 'multiply', '/' => 'divide'];

    private $opMethod;

    public function __construct()
    {
        /**
         * Initialise as an add op
         */
        $this->opMethod = self::$validOps['+'];
    }

    /**
     * Set the type of op we are, will return true if
     * it is a valid type, otherwise will return false.
     *
     * @param string $opCode
     * @return bool
     */
    public function set($opCode)
    {
        if (array_key_exists($opCode, self::$validOps))
        {
            $this->opMethod = self::$validOps[$opCode];
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Return the precedence of the methods, used to determine
     * calculation order.
     *
     * @return int
     */
    public function precedence()
    {
        switch ($this->opMethod)
        {
            case 'multiply':
            case 'divide':
                return 2;
                break;
        }

        return 1;
    }

    /**
     * Run the corresponding method to our currently set op type.
     *
     * @param float $num1
     * @param float $num2
     * @return bool|float
     */
    public function run($num1, $num2)
    {
        if (method_exists($this, $this->opMethod))
        {
            return $this->{$this->opMethod}((float)$num1, (float)$num2);
        }

        return FALSE;
    }

    /**
     * @param float $num1
     * @param float $num2
     * @return float
     */
    private function add($num1, $num2)
    {
        return $num1 + $num2;
    }

    /**
     * @param float $num1
     * @param float $num2
     * @return float
     */
    private function subtract($num1, $num2)
    {
        return $num1 - $num2;
    }

    /**
     * @param float $num1
     * @param float $num2
     * @return float
     */
    private function multiply($num1, $num2)
    {
        return $num1 * $num2;
    }

    /**
     * @param float $num1
     * @param float $num2
     * @return float
     * @throws Exception
     */
    private function divide($num1, $num2)
    {
        if ($num2 === (float)0)
        {
            throw new Exception('Attempt to divide by zero');
        }

        return $num1 / $num2;
    }


}