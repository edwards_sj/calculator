<?php

/**
 * Created by PhpStorm.
 * User: edwards_sj
 * Date: 21/02/2015
 * Time: 11:40
 */
class calculator
{

    /** @var array */
    private $stack = [];
    /** @var array */
    private $opStack = [];
    /** @var array */
    private $numberStack = [];

    /**
     * Push a new item on to the stack, no validation is
     * done at this point, we could check to see if $item
     * was a number or op here.
     *
     * @param string $item
     */
    public function push($item)
    {
        $this->stack[] = $item;
    }

    /**
     * Clear the current stack
     */
    public function clear()
    {
        $this->stack = [];
    }

    /**
     * Run the current stack, will throw an exception if the
     * stack is invalid or return a float value with the result
     * of the calculation.
     *
     * @return float
     * @throws Exception
     */
    public function run()
    {
        $this->numberStack = [];
        $this->opStack     = [];

        foreach ($this->stack as $item)
        {
            /**
             * If item is a number then just throw it on the
             * number stack and move on.
             */
            if (is_numeric($item))
            {
                $this->numberStack[] = (float)$item;
                continue;
            }

            /**
             * Any other item must be a valid op or an
             * exception is raised.
             */
            $op = new op();
            if (!$op->set($item))
            {
                throw new Exception('Invalid stack');
            }

            /**
             * If we hit a lower precedence operation then
             * process all the high precedence operations behind
             * us.
             */
            while ($lastOp = end($this->opStack))
            {
                /** @var op $lastOp */
                if ($op->precedence() <= $lastOp->precedence())
                {
                    $this->runOp();
                }
                else
                {
                    break;
                }
            }

            /**
             * throw the current op on to the op stack.
             */
            $this->opStack[] = $op;
        }

        /**
         * Run any remaining ops
         */
        while (!empty($this->opStack))
        {
            /** @var op $op */
            $this->runOp();
        }

        /**
         * At the end of the calculation we should have one entry in
         * the number stack, anything else is invalid.
         */
        if (count($this->numberStack) != 1)
        {
            throw new Exception('Invalid stack');
        }

        /**
         * Return the result of processing the stack
         */
        return (float)$this->numberStack[0];

    }

    /**
     * Run the last op on the op stack, throwing an
     * exception if it cannot be run.
     *
     * @throws Exception
     */
    public function runOp()
    {
        $num2 = array_pop($this->numberStack);
        $num1 = array_pop($this->numberStack);
        $op   = array_pop($this->opStack);

        if ($op && $num1 && $num2)
        {
            $this->numberStack[] = $op->run($num1, $num2);
        }
        else
        {
            throw new Exception('Invalid stack');
        }
    }

}